$(document).ready(function() {
	$('.user_dropdown').dropdown();
	$('.tabs').tabs();
	$('.sidenav').sidenav({
		edge: "right"
	});
	$('.datepicker').datepicker(); 
	$('.collapsible').collapsible();
	$('select').formSelect(); 
	$('.modal').modal({
		opacity: 0.7
	}); 
	$('.tooltipped').tooltip();

	$('.sidenav-trigger').click(function(){
		$('.sidenav-overlay').css('visibility', 'visible');
	});
}); 
